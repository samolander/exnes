#!/usr/bin/env bash
MASTER="master"
WORKER="worker"
NGINX_IMAGE="nginx:alpine"
HAPROXY_IMAGE="haproxy:alpine"

init() {
    echo "Init docker machines"
    docker-machine create --driver virtualbox ${MASTER} & \
    docker-machine create --driver virtualbox ${WORKER} & \
    wait
    echo "Docker machines already created"
    MASTER_IP=`docker-machine ip ${MASTER}`
    WORKER_IP=`docker-machine ip ${WORKER}`
}

swarm_master() {
    eval $(docker-machine env ${MASTER})
    docker swarm init --advertise-addr ${MASTER_IP}
    TOKEN=`docker swarm join-token -q worker`
    sleep 10
}

swarm_worker() {
	eval $(docker-machine env ${WORKER})
	docker swarm join ${MASTER_IP}:2377 --token ${TOKEN}
}

deploy_containers() {
	eval $(docker-machine env $MASTER )
	docker service create --detach=true --mode global --name nginx --publish 8000:80 --mount type=bind,source=/root,destination=/host nginx:alpine && \
	docker service create --detach=true --mode global --name haproxy -e BACKEND_ADDR_1=${MASTER_IP} -e BACKEND_ADDR_2=${WORKER_IP} --publish 80:80 --mount type=bind,source=/root,destination=/host registry.gitlab.com/samolander/exnes/haproxy
        echo "nginx and haproxy containers already started"
}

check_haproxy_backends() {
    stats=`curl http://${MASTER_IP}/haproxy\?stats\;csv`
    nginx_1_status=`echo "$stats" | grep "^nginx,nginx-1" | cut -d, -f20`
    nginx_2_status=`echo "$stats" | grep "^nginx,nginx-2" | cut -d, -f20`
    echo $stats
    echo ${nginx_1_status}
    echo ${nginx_2_status}
    if [ "$nginx_1_status" == "1" ]; then
    	echo "nginx on master node is available"
    else
    	echo "nginx on master node is not available"
    fi
    if [ "$nginx_2_status" == "1" ]; then
    	echo "nginx on worker node is available"
    else
    	echo "nginx on worker node is not available"
    fi
}

init 
swarm_master
swarm_worker
deploy_containers
check_haproxy_backends
